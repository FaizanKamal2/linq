﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp12
{
    class tblstdinformation
    {
        public int StdId { get; set; }
        public string StdName { get; set; }
        public string StdFatherName { get; set; }
        public int StdContact { get; set; }
        public string StdEmail { get; set; }
        public DateTime CreateDT { get; set; }
        public string CreateBy { get; set; }
        public DateTime UpdateDT { get; set; }
        public string UpdateBy { get; set; }

        public List<tblstdinformation> StudentsDataFunction()//Students Data Function.//
        {
            List<tblstdinformation> LstStudentsData = new List<tblstdinformation>
            {
                new tblstdinformation{StdId = 1,StdName = "Faizan",StdFatherName = "Kamal",StdContact = 0348028,StdEmail = "faizankamal40@gmail.com,",CreateDT = DateTime.Parse("04/02/2018"),CreateBy = "Fa",UpdateDT = DateTime.Parse("10/03/2020"),UpdateBy = "Saqib"},
                new tblstdinformation{StdId = 2,StdName = "Farooq",StdFatherName = " Hanif",StdContact = 0331215,StdEmail = "Farooq@gmail.com,",CreateDT = DateTime.Parse("03/02/2018"),CreateBy = "Hass",UpdateDT = DateTime.Parse("02/02/2018"),UpdateBy = "Ali"},
                new tblstdinformation{StdId = 3,StdName = "Muhammad Saqib",StdFatherName = "Ali",StdContact = 0313101,StdEmail = "Saqib@gmail.com,",CreateDT = DateTime.Parse("09/01/2015"),CreateBy = "Sa",UpdateDT = DateTime.Parse("04/08/2020"),UpdateBy = "Asad"}
            };
            return LstStudentsData;
        }

        public std_getdatabyid GetDataById(int Input)//User Input Id To Get Data Against Id
        {
            std_getdatabyid GetDataClass = new std_getdatabyid();//Another Class Object In which Only 2 Properties assign By Here.

            tblstdinformation DataObj = new tblstdinformation();//This Class Object.

            List<tblstdinformation> LstData = new List<tblstdinformation>();

            LstData = DataObj.StudentsDataFunction();
            DataObj = LstData.Where(x => x.StdId == Input).SingleOrDefault();

            GetDataClass.StudentID = DataObj.StdId;
            GetDataClass.StudentEmail = DataObj.StdEmail;

            return GetDataClass;
        }

        public List<tblstdinformation> GetByMultipleData(int IdInput, string InputName)//Data Get Id And Name If Both Are Same Then Result will Be Shown.
        {
            List<tblstdinformation> MultipleList = new List<tblstdinformation>();
            tblstdinformation StudentData = new tblstdinformation();
            MultipleList = StudentData.StudentsDataFunction();

            MultipleList = MultipleList.Where(x => x.StdId == IdInput && x.StdName == InputName).ToList();
            foreach (tblstdinformation Showdata in MultipleList)
            {
                Console.WriteLine("Student ID : {0}", Showdata.StdId);
                Console.WriteLine("Student Name : {0}", Showdata.StdName);
                Console.WriteLine("Student Father Name : {0}", Showdata.StdFatherName);
                Console.WriteLine("Student Contact No : {0}", Showdata.StdContact);
                Console.WriteLine("Student Email : {0}", Showdata.StdEmail);
                Console.WriteLine("Create Date : {0}", Showdata.CreateDT);
                Console.WriteLine("Create By : {0}", Showdata.CreateBy);
                Console.WriteLine("Update Date : {0}", Showdata.UpdateDT);
                Console.WriteLine("Update By : {0}", Showdata.UpdateBy);

            }

            return MultipleList;
        }
    }
}

    
